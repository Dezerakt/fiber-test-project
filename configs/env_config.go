package configs

import (
	"github.com/rotisserie/eris"
	"github.com/spf13/viper"
)

var (
	EnvConfigObject *EnvConfig

	ErrReadingFromConfigFile = eris.New("ошибка при чтении из config файла")
	ErrUnmarshalConfig       = eris.New("ошибка при парсинге данных из config файла")
)

type EnvConfig struct {
	ServerPort string `mapstructure:"SERVER_PORT"`

	MongoHost     string `mapstructure:"MONGO_HOST"`
	MongoPort     string `mapstructure:"MONGO_PORT"`
	MongoUser     string `mapstructure:"MONGO_USER"`
	MongoPassword string `mapstructure:"MONGO_PASSWORD"`
	MongoDb       string `mapstructure:"MONGO_DB"`
}

func NewEnvConfig() *EnvConfig {
	if EnvConfigObject == nil {
		EnvConfigObject = &EnvConfig{}
	}

	return EnvConfigObject
}

func (config *EnvConfig) LoadConfig() error {
	viper.AddConfigPath(".")
	viper.SetConfigType("env")
	viper.SetConfigName(".env")

	viper.AutomaticEnv()

	err := viper.ReadInConfig()

	if err != nil {
		return err
	}

	err = viper.Unmarshal(config)

	if err != nil {
		return err
	}

	return nil
}
