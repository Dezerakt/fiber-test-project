package configs

import (
	"context"
	"fmt"
	"github.com/fatih/color"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

var (
	MongoObject *mongo.Client = &mongo.Client{}

	TablesList = []interface{}{}
)

type DbConfig struct {
}

func NewDbConfig() *DbConfig {
	return &DbConfig{}
}

func (d *DbConfig) ConnectToMongo(config *EnvConfig) error {
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s:%s", config.MongoUser, config.MongoPassword, config.MongoHost, config.MongoPort))

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return err
	}

	if err := client.Ping(ctx, nil); err != nil {
		return err
	}

	MongoObject = client

	log.Println(color.GreenString("Connection to Mongo successfully"))

	return nil
}
