package books

import (
	"fiber-test-proejct/internal/app/models"
	"fiber-test-proejct/internal/app/repositories"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type DeleteBook struct {
	ID primitive.ObjectID `json:"id"`
}

func NewDeleteBook() *DeleteBook {
	return &DeleteBook{}
}

func (d *DeleteBook) Execute() (*mongo.DeleteResult, error) {
	bookRepository := repositories.NewBooksRepository()
	result, err := bookRepository.DeleteBook(&models.Book{
		ID: d.ID,
	})
	if err != nil {
		return nil, err
	}

	return result, nil
}
