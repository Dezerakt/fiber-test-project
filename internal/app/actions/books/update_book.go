package books

import (
	"fiber-test-proejct/internal/app/models"
	"fiber-test-proejct/internal/app/repositories"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type UpdateBook struct {
	ID   primitive.ObjectID `json:"id"`
	Name string             `json:"name"`
}

func NewUpdateBook() *UpdateBook {
	return &UpdateBook{}
}

func (u *UpdateBook) Execute() (*mongo.UpdateResult, error) {
	bookRepository := repositories.NewBooksRepository()
	result, err := bookRepository.UpdateBook(&models.Book{
		ID:   u.ID,
		Name: u.Name,
	})
	if err != nil {
		return nil, err
	}

	return result, nil
}
