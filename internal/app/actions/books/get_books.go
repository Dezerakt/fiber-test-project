package books

import (
	"fiber-test-proejct/internal/app/repositories"
	"github.com/rotisserie/eris"
)

type GetBooks struct {
}

func NewGetBooks() *GetBooks {
	return &GetBooks{}
}

func (b *GetBooks) Execute() (interface{}, error) {
	BookRepository := repositories.NewBooksRepository()

	movies, err := BookRepository.GetMovies()
	if err != nil {
		return nil, eris.Errorf(err.Error())
	}

	return movies, nil
}
