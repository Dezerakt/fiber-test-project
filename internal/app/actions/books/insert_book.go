package books

import (
	"fiber-test-proejct/internal/app/models"
	"fiber-test-proejct/internal/app/repositories"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type InsertOneBook struct {
	Name string `json:"name" binding:"required"`
}

func NewInsertOneBook() *InsertOneBook {
	return &InsertOneBook{}
}

func (i *InsertOneBook) Execute() (*mongo.InsertOneResult, error) {
	BookRepository := repositories.NewBooksRepository()
	insertOneResult, err := BookRepository.InsertOne(&models.Book{
		ID:   primitive.NewObjectID(),
		Name: i.Name,
	})
	if err != nil {
		return nil, err
	}

	return insertOneResult, nil
}
