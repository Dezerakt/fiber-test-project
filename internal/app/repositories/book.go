package repositories

import (
	"context"
	"fiber-test-proejct/configs"
	"fiber-test-proejct/internal/app/models"
	"github.com/rotisserie/eris"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BookRepository struct {
	CollectionObject *mongo.Collection

	*MainRepository
}

func NewBooksRepository() *BookRepository {
	return &BookRepository{
		CollectionObject: configs.MongoObject.Database(configs.EnvConfigObject.MongoDb).Collection("books"),
		MainRepository:   NewMainRepository(),
	}
}

/*func (u *BookRepository) InsertOne(item *models.Book) (*models.Book, error) {
	item.ID = primitive.NewObjectID()
	_, err := d.Collection.InsertOne(context.Background(), item)
	if err != nil {
		return nil, err
	}

	return item, nil
}*/

func (b *BookRepository) GetMovies() (interface{}, error) {
	cursor, err := b.CollectionObject.Find(context.TODO(), bson.D{{}})

	var movies []bson.M
	if err = cursor.All(context.TODO(), &movies); err != nil {
		return nil, eris.Errorf(err.Error())
	}

	return movies, nil
}

func (b *BookRepository) InsertOne(target *models.Book) (*mongo.InsertOneResult, error) {
	result, err := b.CollectionObject.InsertOne(context.TODO(), target)
	if err != nil {
		return nil, eris.Errorf(err.Error())
	}

	return result, nil
}

func (b *BookRepository) UpdateBook(target *models.Book) (*mongo.UpdateResult, error) {
	id, _ := primitive.ObjectIDFromHex(target.ID.Hex())
	update := bson.D{{"$set", bson.D{{"name", target.Name}}}}
	filter := bson.D{{"_id", id}}

	result, err := b.CollectionObject.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (b *BookRepository) DeleteBook(target *models.Book) (*mongo.DeleteResult, error) {
	filter := bson.D{{"_id", target.ID}}
	result, err := b.CollectionObject.DeleteOne(context.TODO(), filter)
	// Prints a message if any errors occur during the operation
	if err != nil {
		return nil, err
	}

	return result, nil
}
