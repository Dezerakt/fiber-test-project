package handlers

import (
	"encoding/json"
	"fiber-test-proejct/internal/app/actions/books"
	"github.com/gofiber/fiber/v2"
	"github.com/rotisserie/eris"
)

type MainHandler struct {
}

func NewMainHandler() *MainHandler {
	return &MainHandler{}
}

func (m *MainHandler) GetBooks(c *fiber.Ctx) error {
	getBooks := books.NewGetBooks()

	movies, err := getBooks.Execute()
	if err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	return c.JSON(map[string]interface{}{
		"result": movies,
	})
}

func (m *MainHandler) Health(c *fiber.Ctx) error {
	return c.JSON(map[string]interface{}{
		"result": "done",
	})
}

func (m *MainHandler) InsertBook(c *fiber.Ctx) error {
	insertBook := books.NewInsertOneBook()

	if err := json.Unmarshal(c.Body(), &insertBook); err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	result, err := insertBook.Execute()
	if err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	return c.JSON(map[string]interface{}{
		"result": "done",
		"data":   result,
	})
}

func (m *MainHandler) UpdateDocument(c *fiber.Ctx) error {
	updateBook := books.NewUpdateBook()

	if err := json.Unmarshal(c.Body(), &updateBook); err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	result, err := updateBook.Execute()
	if err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	return c.JSON(map[string]interface{}{
		"result": "done",
		"data":   result,
	})
}

func (m *MainHandler) DeleteBook(c *fiber.Ctx) error {
	deleteBook := books.NewDeleteBook()

	if err := json.Unmarshal(c.Body(), &deleteBook); err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	result, err := deleteBook.Execute()
	if err != nil {
		return c.JSON(map[string]interface{}{
			"result": "error",
			"error":  eris.Unpack(err),
		})
	}

	return c.JSON(map[string]interface{}{
		"result": "done",
		"data":   result,
	})
}
