package internal

import (
	"fiber-test-proejct/internal/app/handlers"
	"github.com/gofiber/fiber/v2"
)

type RouteHandler struct {
	*handlers.MainHandler
}

func NewRouteHandler() *RouteHandler {
	return &RouteHandler{
		MainHandler: handlers.NewMainHandler(),
	}
}

func (r *RouteHandler) Handle(app *fiber.App) {
	api := app.Group("/api")
	{
		api.Get("/health", r.MainHandler.Health)

		book := api.Group("/book")
		{
			book.Get("", r.MainHandler.GetBooks)
			book.Post("", r.MainHandler.InsertBook)
			book.Put("", r.MainHandler.UpdateDocument)
			book.Delete("", r.MainHandler.DeleteBook)
		}
	}
}
