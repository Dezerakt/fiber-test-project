package main

import (
	"fiber-test-proejct/configs"
	"fiber-test-proejct/internal"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"log"
)

var (
	routeHandler = internal.NewRouteHandler()
	configLoader = configs.NewConfigLoader()
)

func main() {
	if err := configLoader.LoadConfig(); err != nil {
		log.Fatalln(err)
	}

	app := fiber.New()
	routeHandler.Handle(app)

	if err := app.Listen(fmt.Sprintf(":%s", configs.EnvConfigObject.ServerPort)); err != nil {
		log.Fatalln(err)
	}
}
